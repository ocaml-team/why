/**************************************************************************/
/*                                                                        */
/*  The Why platform for program certification                            */
/*                                                                        */
/*  Copyright (C) 2002-2017                                               */
/*                                                                        */
/*    Jean-Christophe FILLIATRE, CNRS & Univ. Paris-sud                   */
/*    Claude MARCHE, INRIA & Univ. Paris-sud                              */
/*    Yannick MOY, Univ. Paris-sud                                        */
/*    Romain BARDOU, Univ. Paris-sud                                      */
/*                                                                        */
/*  Secondary contributors:                                               */
/*                                                                        */
/*    Thierry HUBERT, Univ. Paris-sud  (former Caduceus front-end)        */
/*    Nicolas ROUSSET, Univ. Paris-sud (on Jessie & Krakatoa)             */
/*    Ali AYAD, CNRS & CEA Saclay      (floating-point support)           */
/*    Sylvie BOLDO, INRIA              (floating-point support)           */
/*    Jean-Francois COUCHOT, INRIA     (sort encodings, hyps pruning)     */
/*    Mehdi DOGGUY, Univ. Paris-sud    (Why GUI)                          */
/*                                                                        */
/*  This software is free software; you can redistribute it and/or        */
/*  modify it under the terms of the GNU Lesser General Public            */
/*  License version 2.1, with the special exception on linking            */
/*  described in file LICENSE.                                            */
/*                                                                        */
/*  This software is distributed in the hope that it will be useful,      */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  */
/**************************************************************************/


/*@ axiomatic MKP {
  @  predicate mkp(double uc, double up, integer n);
  @ }
  @*/


/*@ requires 2 <= N <= 0x4000000; // 2^{26}
  @ requires \exact(u0) == u0 && \exact(u1) == u1;
  @ requires \forall integer k; 0 <= k <= N ==>  \abs(u0 + k*(u1-u0)) <= 1.0;
  @ ensures \exact(\result) == u0 + N*(u1-u0);
  @ ensures \round_error(\result) <= N * (N+1.0) / 2.0 * 0x1p-53;
  @*/
double comput_seq(double u0, double u1, int N) {
  int i;
  double uprev, ucur,tmp;
  uprev = u0;
  ucur = u1;

  /*@ loop invariant 2 <= i <= N+1;
    @ loop invariant \exact(ucur) ==u0+(i-1)*(u1-u0);
    @ loop invariant \exact(uprev)==u0+(i-2)*(u1-u0);
    @ loop invariant mkp(ucur,uprev,i-2);
    @ loop variant N-i;
    @*/
  for (i=2; i<=N; i++) {
    tmp=2*ucur;
    //@ assert tmp == 2.0 * ucur;
    tmp -= uprev;
    uprev =ucur;
    ucur = tmp;
  }
  return ucur;
}

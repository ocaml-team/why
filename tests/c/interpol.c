
#define EPS0 0x1p-50
#define EPS1 0x48p-50

#define UPPER 0x1p3

//@ lemma distr_mul_div: \forall real x,y,z; z != 0.0 ==> (x*y)/z == x*(y/z);

//@ lemma round_add_zero: \forall double x; \round_double(\NearestEven,x+0.0) == x;
//@ lemma round_mul_zero: \forall real x; \round_double(\NearestEven,x*0.0) == 0.0;

//@ predicate bounded(real z, real bound) = \abs(z) <= bound;

//@ ghost int i_f;

/*@ requires 3 <= n <= 17;
    requires \valid(X+(0..n));
    requires \valid(Y+(0..n));
    requires \valid(V+(0..n));

    requires bounded(x,UPPER);
    requires \forall integer k; 0 <= k <= n ==> bounded(X[k],UPPER);
    requires \forall integer k; 0 <= k <= n ==> bounded(Y[k],UPPER);
    requires \forall integer k; 0 <= k <= n ==> bounded(V[k],UPPER);

    requires X[0] == X[1];
    requires \forall integer k1,k2; 1 <= k1 < k2 <= n ==> X[k1] < X[k2];

    requires Y[0] == Y[1];
    requires V[0] == V[n] == 0.0;
    requires \forall integer k; 1 <= k <= n-1 ==>
         \abs(V[k] - (Y[k+1] - Y[k]) / (X[k+1] - X[k])) <= EPS0;

    behavior small:
        assumes x < X[0];
        ensures \result == Y[0];

    behavior big:
        assumes x >= X[n];
        ensures \result == Y[n];

    behavior inter:
        assumes X[0] <= x < X[n];
        ensures 1 <= i_f <= n-1 &&
	   \abs(\result - (Y[i_f] + (x-X[i_f]) * (Y[i_f+1] - Y[i_f]) / (X[i_f+1] - X[i_f]))) <=
	     EPS1 ;
*/
double InterPolate(double x, double X[], double Y[], double V[], int n) {
    int i=0;
    //@ ghost int i0;
    int m=(n+1)/2;
    //@ assert 2 <= m <= 9;
    //@ assert m <= n;

    if (x >= X[m]) i=m;
    //@ ghost i0=i;
    //@ for small: assert i==0;

    /*@ loop invariant i0 <= i <= n;
        for inter: loop invariant x >= X[i];
        for small: loop invariant i==0;
        loop variant n-i;
    */
    while ((i < n) && (x >= X[i+1])) i++;
    //@ ghost i_f=i;
    //@ for small: assert i==0;
    //@ for big: assert i < n ==> x < X[i+1];
    //@ for big: assert i==n;
    //@ assert bounded(x,UPPER);
    //@ assert bounded(X[i],UPPER);
    //@ assert bounded(Y[i],UPPER);
    //@ assert bounded(V[i],UPPER);
    //@ for inter: assert \abs(V[i] - (Y[i+1] - Y[i]) / (X[i+1] - X[i])) <= EPS0;
    //@ for small: assert V[i] == 0.0;
    //@ for big: assert V[i] == 0.0;
    double r = (x-X[i]) * V[i];
    //@ for small: assert r == 0.0;
    //@ for big: assert r == 0.0;
    r = Y[i] + r;
    //@ for small: assert r == Y[i];
    //@ for inter: assert \abs(r - (Y[i] + (x-X[i]) * ((Y[i+1] - Y[i]) / (X[i+1] - X[i])))) <= EPS1 ;
    //@ for inter: assert X[i+1] - X[i] != 0.0;
    //@ for inter: assert \abs(r - (Y[i] + (x-X[i]) * (Y[i+1] - Y[i]) / (X[i+1] - X[i]))) <= EPS1;
    //@ for big: assert r == Y[i];
    return r;
}



/*i
Local Variables:
compile-command: "frama-c -jessie interpol.c"
End:
*/

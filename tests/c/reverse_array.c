/**************************************************************************/
/*                                                                        */
/*  The Why platform for program certification                            */
/*                                                                        */
/*  Copyright (C) 2002-2017                                               */
/*                                                                        */
/*    Jean-Christophe FILLIATRE, CNRS & Univ. Paris-sud                   */
/*    Claude MARCHE, INRIA & Univ. Paris-sud                              */
/*    Yannick MOY, Univ. Paris-sud                                        */
/*    Romain BARDOU, Univ. Paris-sud                                      */
/*                                                                        */
/*  Secondary contributors:                                               */
/*                                                                        */
/*    Thierry HUBERT, Univ. Paris-sud  (former Caduceus front-end)        */
/*    Nicolas ROUSSET, Univ. Paris-sud (on Jessie & Krakatoa)             */
/*    Ali AYAD, CNRS & CEA Saclay      (floating-point support)           */
/*    Sylvie BOLDO, INRIA              (floating-point support)           */
/*    Jean-Francois COUCHOT, INRIA     (sort encodings, hyps pruning)     */
/*    Mehdi DOGGUY, Univ. Paris-sud    (Why GUI)                          */
/*                                                                        */
/*  This software is free software; you can redistribute it and/or        */
/*  modify it under the terms of the GNU Lesser General Public            */
/*  License version 2.1, with the special exception on linking            */
/*  described in file LICENSE.                                            */
/*                                                                        */
/*  This software is distributed in the hope that it will be useful,      */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  */
/**************************************************************************/

/** Reversing the elements of an array.
    Original idea by Francesco Bonnizzi <fbonizzi@hotmail.it>
*/

#pragma SeparationPolicy(none)

/*@ requires \valid(a);
  @ requires \valid(b);
  @ assigns(*a);
  @ assigns(*b);
  @ ensures *a == \old(*b);
  @ ensures *b == \old(*a);
  @*/
void swap(char *a, char *b) {
  const char tmp = *a;
  *a = *b;
  *b = tmp;
}

/*@ requires n >= 0;
  @ requires \valid(s+(0 .. n-1));
  @ decreases n;
  @ assigns s[0..(n-1)];
  @ ensures \forall integer i;
  @   0 <= i < n ==> \at(s[i], Pre) == \at(s[n-i-1], Post);
  @*/
void reverse(char *s, unsigned n) {
  if (n < 2) return;
  swap(s, s + (n - 1));
  reverse(s + 1, n - 2);
  //@ assert s[0] == \at(s[n-1], Pre);
  //@ assert s[n-1] == \at(s[0], Pre);
  //@ assert \forall integer i; 1 <= i < n-1 ==> s[i] == (s+1)[i-1] ;
}

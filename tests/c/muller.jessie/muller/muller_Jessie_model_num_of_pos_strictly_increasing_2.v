(* This file is generated by Why3's Coq driver *)
(* Beware! Only edit allowed sections below    *)
Require Import BuiltIn.
Require BuiltIn.
Require int.Int.
Require bool.Bool.
Require Jessie_memory_model.

Axiom charP : Type.
Parameter charP_WhyType : WhyType charP.
Existing Instance charP_WhyType.

Axiom int32 : Type.
Parameter int32_WhyType : WhyType int32.
Existing Instance int32_WhyType.

Axiom int8 : Type.
Parameter int8_WhyType : WhyType int8.
Existing Instance int8_WhyType.

Axiom intP : Type.
Parameter intP_WhyType : WhyType intP.
Existing Instance intP_WhyType.

Axiom padding : Type.
Parameter padding_WhyType : WhyType padding.
Existing Instance padding_WhyType.

Axiom uint32 : Type.
Parameter uint32_WhyType : WhyType uint32.
Existing Instance uint32_WhyType.

Axiom voidP : Type.
Parameter voidP_WhyType : WhyType voidP.
Existing Instance voidP_WhyType.

Parameter charP_tag: (Jessie_memory_model.tag_id charP).

Axiom charP_int : ((Jessie_memory_model.int_of_tag charP_tag) = 1%Z).

Parameter charP_of_pointer_address: (Jessie_memory_model.pointer unit) ->
  (Jessie_memory_model.pointer charP).

Axiom charP_of_pointer_address_of_pointer_addr : forall (p:(Jessie_memory_model.pointer
  charP)),
  (p = (charP_of_pointer_address (Jessie_memory_model.pointer_address p))).

Axiom charP_parenttag_bottom : (Jessie_memory_model.parenttag charP_tag
  (Jessie_memory_model.bottom_tag : (Jessie_memory_model.tag_id charP))).

Axiom charP_tags : forall (x:(Jessie_memory_model.pointer charP)),
  forall (charP_tag_table:(Jessie_memory_model.tag_table charP)),
  (Jessie_memory_model.instanceof charP_tag_table x charP_tag).

Parameter integer_of_int32: int32 -> Z.

(* Why3 assumption *)
Definition eq_int32 (x:int32) (y:int32): Prop :=
  ((integer_of_int32 x) = (integer_of_int32 y)).

Parameter integer_of_int8: int8 -> Z.

(* Why3 assumption *)
Definition eq_int8 (x:int8) (y:int8): Prop :=
  ((integer_of_int8 x) = (integer_of_int8 y)).

Parameter integer_of_uint32: uint32 -> Z.

(* Why3 assumption *)
Definition eq_uint32 (x:uint32) (y:uint32): Prop :=
  ((integer_of_uint32 x) = (integer_of_uint32 y)).

Parameter int32_of_integer: Z -> int32.

Axiom int32_coerce : forall (x:Z), (((-2147483648%Z)%Z <= x)%Z /\
  (x <= 2147483647%Z)%Z) -> ((integer_of_int32 (int32_of_integer x)) = x).

Axiom int32_extensionality : forall (x:int32), forall (y:int32),
  ((integer_of_int32 x) = (integer_of_int32 y)) -> (x = y).

Axiom int32_range : forall (x:int32),
  ((-2147483648%Z)%Z <= (integer_of_int32 x))%Z /\
  ((integer_of_int32 x) <= 2147483647%Z)%Z.

Parameter int8_of_integer: Z -> int8.

Axiom int8_coerce : forall (x:Z), (((-128%Z)%Z <= x)%Z /\ (x <= 127%Z)%Z) ->
  ((integer_of_int8 (int8_of_integer x)) = x).

Axiom int8_extensionality : forall (x:int8), forall (y:int8),
  ((integer_of_int8 x) = (integer_of_int8 y)) -> (x = y).

Axiom int8_range : forall (x:int8), ((-128%Z)%Z <= (integer_of_int8 x))%Z /\
  ((integer_of_int8 x) <= 127%Z)%Z.

Parameter intP_tag: (Jessie_memory_model.tag_id intP).

Axiom intP_int : ((Jessie_memory_model.int_of_tag intP_tag) = 1%Z).

Parameter intP_of_pointer_address: (Jessie_memory_model.pointer unit) ->
  (Jessie_memory_model.pointer intP).

Axiom intP_of_pointer_address_of_pointer_addr : forall (p:(Jessie_memory_model.pointer
  intP)),
  (p = (intP_of_pointer_address (Jessie_memory_model.pointer_address p))).

Axiom intP_parenttag_bottom : (Jessie_memory_model.parenttag intP_tag
  (Jessie_memory_model.bottom_tag : (Jessie_memory_model.tag_id intP))).

Axiom intP_tags : forall (x:(Jessie_memory_model.pointer intP)),
  forall (intP_tag_table:(Jessie_memory_model.tag_table intP)),
  (Jessie_memory_model.instanceof intP_tag_table x intP_tag).

(* Why3 assumption *)
Definition left_valid_struct_charP (p:(Jessie_memory_model.pointer charP))
  (a:Z) (charP_alloc_table:(Jessie_memory_model.alloc_table charP)): Prop :=
  ((Jessie_memory_model.offset_min charP_alloc_table p) <= a)%Z.

(* Why3 assumption *)
Definition left_valid_struct_intP (p:(Jessie_memory_model.pointer intP))
  (a:Z) (intP_alloc_table:(Jessie_memory_model.alloc_table intP)): Prop :=
  ((Jessie_memory_model.offset_min intP_alloc_table p) <= a)%Z.

(* Why3 assumption *)
Definition left_valid_struct_voidP (p:(Jessie_memory_model.pointer voidP))
  (a:Z) (voidP_alloc_table:(Jessie_memory_model.alloc_table voidP)): Prop :=
  ((Jessie_memory_model.offset_min voidP_alloc_table p) <= a)%Z.

Parameter num_of_pos: Z -> Z -> (Jessie_memory_model.pointer intP) ->
  (Jessie_memory_model.memory intP int32) -> Z.

Axiom pointer_addr_of_charP_of_pointer_address : forall (p:(Jessie_memory_model.pointer
  unit)),
  (p = (Jessie_memory_model.pointer_address (charP_of_pointer_address p))).

Axiom pointer_addr_of_intP_of_pointer_address : forall (p:(Jessie_memory_model.pointer
  unit)),
  (p = (Jessie_memory_model.pointer_address (intP_of_pointer_address p))).

Parameter voidP_of_pointer_address: (Jessie_memory_model.pointer unit) ->
  (Jessie_memory_model.pointer voidP).

Axiom pointer_addr_of_voidP_of_pointer_address : forall (p:(Jessie_memory_model.pointer
  unit)),
  (p = (Jessie_memory_model.pointer_address (voidP_of_pointer_address p))).

(* Why3 assumption *)
Definition right_valid_struct_charP (p:(Jessie_memory_model.pointer charP))
  (b:Z) (charP_alloc_table:(Jessie_memory_model.alloc_table charP)): Prop :=
  (b <= (Jessie_memory_model.offset_max charP_alloc_table p))%Z.

(* Why3 assumption *)
Definition right_valid_struct_intP (p:(Jessie_memory_model.pointer intP))
  (b:Z) (intP_alloc_table:(Jessie_memory_model.alloc_table intP)): Prop :=
  (b <= (Jessie_memory_model.offset_max intP_alloc_table p))%Z.

(* Why3 assumption *)
Definition right_valid_struct_voidP (p:(Jessie_memory_model.pointer voidP))
  (b:Z) (voidP_alloc_table:(Jessie_memory_model.alloc_table voidP)): Prop :=
  (b <= (Jessie_memory_model.offset_max voidP_alloc_table p))%Z.

(* Why3 assumption *)
Definition strict_valid_root_charP (p:(Jessie_memory_model.pointer charP))
  (a:Z) (b:Z) (charP_alloc_table:(Jessie_memory_model.alloc_table
  charP)): Prop := ((Jessie_memory_model.offset_min charP_alloc_table
  p) = a) /\ ((Jessie_memory_model.offset_max charP_alloc_table p) = b).

(* Why3 assumption *)
Definition strict_valid_root_intP (p:(Jessie_memory_model.pointer intP))
  (a:Z) (b:Z) (intP_alloc_table:(Jessie_memory_model.alloc_table
  intP)): Prop := ((Jessie_memory_model.offset_min intP_alloc_table
  p) = a) /\ ((Jessie_memory_model.offset_max intP_alloc_table p) = b).

(* Why3 assumption *)
Definition strict_valid_root_voidP (p:(Jessie_memory_model.pointer voidP))
  (a:Z) (b:Z) (voidP_alloc_table:(Jessie_memory_model.alloc_table
  voidP)): Prop := ((Jessie_memory_model.offset_min voidP_alloc_table
  p) = a) /\ ((Jessie_memory_model.offset_max voidP_alloc_table p) = b).

(* Why3 assumption *)
Definition strict_valid_struct_charP (p:(Jessie_memory_model.pointer charP))
  (a:Z) (b:Z) (charP_alloc_table:(Jessie_memory_model.alloc_table
  charP)): Prop := ((Jessie_memory_model.offset_min charP_alloc_table
  p) = a) /\ ((Jessie_memory_model.offset_max charP_alloc_table p) = b).

(* Why3 assumption *)
Definition strict_valid_struct_intP (p:(Jessie_memory_model.pointer intP))
  (a:Z) (b:Z) (intP_alloc_table:(Jessie_memory_model.alloc_table
  intP)): Prop := ((Jessie_memory_model.offset_min intP_alloc_table
  p) = a) /\ ((Jessie_memory_model.offset_max intP_alloc_table p) = b).

(* Why3 assumption *)
Definition strict_valid_struct_voidP (p:(Jessie_memory_model.pointer voidP))
  (a:Z) (b:Z) (voidP_alloc_table:(Jessie_memory_model.alloc_table
  voidP)): Prop := ((Jessie_memory_model.offset_min voidP_alloc_table
  p) = a) /\ ((Jessie_memory_model.offset_max voidP_alloc_table p) = b).

Parameter uint32_of_integer: Z -> uint32.

Axiom uint32_coerce : forall (x:Z), ((0%Z <= x)%Z /\
  (x <= 4294967295%Z)%Z) -> ((integer_of_uint32 (uint32_of_integer x)) = x).

Axiom uint32_extensionality : forall (x:uint32), forall (y:uint32),
  ((integer_of_uint32 x) = (integer_of_uint32 y)) -> (x = y).

Axiom uint32_range : forall (x:uint32), (0%Z <= (integer_of_uint32 x))%Z /\
  ((integer_of_uint32 x) <= 4294967295%Z)%Z.

(* Why3 assumption *)
Definition valid_root_charP (p:(Jessie_memory_model.pointer charP)) (a:Z)
  (b:Z) (charP_alloc_table:(Jessie_memory_model.alloc_table charP)): Prop :=
  ((Jessie_memory_model.offset_min charP_alloc_table p) <= a)%Z /\
  (b <= (Jessie_memory_model.offset_max charP_alloc_table p))%Z.

(* Why3 assumption *)
Definition valid_root_intP (p:(Jessie_memory_model.pointer intP)) (a:Z) (b:Z)
  (intP_alloc_table:(Jessie_memory_model.alloc_table intP)): Prop :=
  ((Jessie_memory_model.offset_min intP_alloc_table p) <= a)%Z /\
  (b <= (Jessie_memory_model.offset_max intP_alloc_table p))%Z.

(* Why3 assumption *)
Definition valid_root_voidP (p:(Jessie_memory_model.pointer voidP)) (a:Z)
  (b:Z) (voidP_alloc_table:(Jessie_memory_model.alloc_table voidP)): Prop :=
  ((Jessie_memory_model.offset_min voidP_alloc_table p) <= a)%Z /\
  (b <= (Jessie_memory_model.offset_max voidP_alloc_table p))%Z.

(* Why3 assumption *)
Definition valid_struct_charP (p:(Jessie_memory_model.pointer charP)) (a:Z)
  (b:Z) (charP_alloc_table:(Jessie_memory_model.alloc_table charP)): Prop :=
  ((Jessie_memory_model.offset_min charP_alloc_table p) <= a)%Z /\
  (b <= (Jessie_memory_model.offset_max charP_alloc_table p))%Z.

(* Why3 assumption *)
Definition valid_struct_intP (p:(Jessie_memory_model.pointer intP)) (a:Z)
  (b:Z) (intP_alloc_table:(Jessie_memory_model.alloc_table intP)): Prop :=
  ((Jessie_memory_model.offset_min intP_alloc_table p) <= a)%Z /\
  (b <= (Jessie_memory_model.offset_max intP_alloc_table p))%Z.

(* Why3 assumption *)
Definition valid_struct_voidP (p:(Jessie_memory_model.pointer voidP)) (a:Z)
  (b:Z) (voidP_alloc_table:(Jessie_memory_model.alloc_table voidP)): Prop :=
  ((Jessie_memory_model.offset_min voidP_alloc_table p) <= a)%Z /\
  (b <= (Jessie_memory_model.offset_max voidP_alloc_table p))%Z.

Parameter voidP_tag: (Jessie_memory_model.tag_id voidP).

Axiom voidP_int : ((Jessie_memory_model.int_of_tag voidP_tag) = 1%Z).

Axiom voidP_of_pointer_address_of_pointer_addr : forall (p:(Jessie_memory_model.pointer
  voidP)),
  (p = (voidP_of_pointer_address (Jessie_memory_model.pointer_address p))).

Axiom voidP_parenttag_bottom : (Jessie_memory_model.parenttag voidP_tag
  (Jessie_memory_model.bottom_tag : (Jessie_memory_model.tag_id voidP))).

Axiom voidP_tags : forall (x:(Jessie_memory_model.pointer voidP)),
  forall (voidP_tag_table:(Jessie_memory_model.tag_table voidP)),
  (Jessie_memory_model.instanceof voidP_tag_table x voidP_tag).

Axiom num_of_pos_empty : forall (intP_intM_t_3_at_L:(Jessie_memory_model.memory
  intP int32)), forall (i_0:Z), forall (j_0:Z),
  forall (t_0_0:(Jessie_memory_model.pointer intP)), (j_0 <= i_0)%Z ->
  ((num_of_pos i_0 j_0 t_0_0 intP_intM_t_3_at_L) = 0%Z).

Axiom num_of_pos_true_case : forall (intP_intM_t_3_at_L:(Jessie_memory_model.memory
  intP int32)), forall (i_1:Z), forall (j_1:Z),
  forall (t_1:(Jessie_memory_model.pointer intP)), ((i_1 < j_1)%Z /\
  (0%Z < (integer_of_int32 (Jessie_memory_model.select intP_intM_t_3_at_L
  (Jessie_memory_model.shift t_1 (j_1 - 1%Z)%Z))))%Z) -> ((num_of_pos i_1 j_1
  t_1 intP_intM_t_3_at_L) = ((num_of_pos i_1 (j_1 - 1%Z)%Z t_1
  intP_intM_t_3_at_L) + 1%Z)%Z).

Axiom num_of_pos_false_case : forall (intP_intM_t_3_at_L:(Jessie_memory_model.memory
  intP int32)), forall (i_2:Z), forall (j_2:Z),
  forall (t_2:(Jessie_memory_model.pointer intP)), ((i_2 < j_2)%Z /\
  ~ (0%Z < (integer_of_int32 (Jessie_memory_model.select intP_intM_t_3_at_L
  (Jessie_memory_model.shift t_2 (j_2 - 1%Z)%Z))))%Z) -> ((num_of_pos i_2 j_2
  t_2 intP_intM_t_3_at_L) = (num_of_pos i_2 (j_2 - 1%Z)%Z t_2
  intP_intM_t_3_at_L)).

Axiom num_of_pos_non_negative : forall (intP_intM_t_3_10_at_L:(Jessie_memory_model.memory
  intP int32)), forall (i_3:Z), forall (j_3:Z),
  forall (t_3:(Jessie_memory_model.pointer intP)), (0%Z <= (num_of_pos i_3
  j_3 t_3 intP_intM_t_3_10_at_L))%Z.

Axiom num_of_pos_additive : forall (intP_intM_t_4_11_at_L:(Jessie_memory_model.memory
  intP int32)), forall (i_4:Z), forall (j_4:Z), forall (k:Z),
  forall (t_4:(Jessie_memory_model.pointer intP)), ((i_4 <= j_4)%Z /\
  (j_4 <= k)%Z) -> ((num_of_pos i_4 k t_4
  intP_intM_t_4_11_at_L) = ((num_of_pos i_4 j_4 t_4
  intP_intM_t_4_11_at_L) + (num_of_pos j_4 k t_4 intP_intM_t_4_11_at_L))%Z).

Axiom num_of_pos_increasing : forall (intP_intM_t_5_12_at_L:(Jessie_memory_model.memory
  intP int32)), forall (i_5:Z), forall (j_5:Z), forall (k_0:Z),
  forall (t_5:(Jessie_memory_model.pointer intP)), (j_5 <= k_0)%Z ->
  ((num_of_pos i_5 j_5 t_5 intP_intM_t_5_12_at_L) <= (num_of_pos i_5 k_0 t_5
  intP_intM_t_5_12_at_L))%Z.

Open Scope Z_scope.

(* Why3 goal *)
Theorem num_of_pos_strictly_increasing : forall (intP_intM_t_6_13_at_L:(Jessie_memory_model.memory
  intP int32)), forall (i_6:Z), forall (n:Z),
  forall (t_6:(Jessie_memory_model.pointer intP)), ((0%Z <= i_6)%Z /\
  ((i_6 < n)%Z /\
  (0%Z < (integer_of_int32 (Jessie_memory_model.select intP_intM_t_6_13_at_L
  (Jessie_memory_model.shift t_6 i_6))))%Z)) -> ((num_of_pos 0%Z i_6 t_6
  intP_intM_t_6_13_at_L) < (num_of_pos 0%Z n t_6 intP_intM_t_6_13_at_L))%Z.
(* Why3 intros intP_intM_t_6_13_at_L i_6 n t_6 (h1,(h2,h3)). *)
(* intros intP_intM_t_6_13_at_L i_8 n t_6 (h1,(h2,h3)). *)
(* YOU MAY EDIT THE PROOF BELOW *)
intros tMem i n t (Hi, (Hin, H)).
rewrite num_of_pos_additive with (k:=n) (j_4:= i); auto with zarith.
rewrite num_of_pos_additive with (k:=n) (j_4:= i+1); auto with zarith.
rewrite num_of_pos_true_case with (i_1:=i); 
  replace ((i + 1) - 1) with i by omega; 
  intuition.
generalize (num_of_pos_non_negative tMem 0 i t); intro.
generalize (num_of_pos_non_negative tMem i i t); intro.
generalize (num_of_pos_non_negative tMem (i+1) n t); intro.
omega.
Qed.


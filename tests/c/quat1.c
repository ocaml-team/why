#pragma JessieFloatModel(math)
#pragma JessieTerminationPolicy(user)



/*********** Quaternions: types, equality, copy *************/


typedef double quat[4];
typedef double *quat_ptr;

// equality of two quaternions in two different memory states

/*@ predicate quat_eq{L1,L2}(quat_ptr q1, quat_ptr q2) =
  @    \at(q1[0],L1) == \at(q2[0],L2) && \at(q1[1],L1) == \at(q2[1],L2)
  @ && \at(q1[2],L1) == \at(q2[2],L2) && \at(q1[3],L1) == \at(q2[3],L2);
  @*/


/*@ requires \valid(src+(0..3));
  @ requires \valid(dst+(0..3));
  @ assigns dst[0..3];
  @ ensures quat_eq{Here,Old}(dst,src);
  @*/
void Quat_copy(const quat src,quat dst) {
  dst[0] = src[0];
  dst[1] = src[1];
  dst[2] = src[2];
  dst[3] = src[3];
}


/*************** Quaternions: product ******************/


/*@ logic real product1{L}(quat_ptr q1, quat_ptr q2) =
  @    q1[0]*q2[0] - q1[1]*q2[1] - q1[2]*q2[2] - q1[3]*q2[3] ;
  @
  @ logic real product2{L}(quat_ptr q1, quat_ptr q2) =
  @    q1[0]*q2[1] + q1[1]*q2[0] + q1[2]*q2[3] - q1[3]*q2[2] ;
  @
  @ logic real product3{L}(quat_ptr q1, quat_ptr q2) =
  @    q1[0]*q2[2] - q1[1]*q2[3] + q1[2]*q2[0] + q1[3]*q2[1] ;
  @
  @ logic real product4{L}(quat_ptr q1, quat_ptr q2) =
  @    q1[0]*q2[3] + q1[1]*q2[2] - q1[2]*q2[1] + q1[3]*q2[0] ;
  @*/


/*@ predicate is_product{L}(quat_ptr q1, quat_ptr q2, quat_ptr q) =
  @    q[0] == product1(q1,q2) && q[1] == product2(q1,q2) 
  @ && q[2] == product3(q1,q2) && q[3] == product4(q1,q2);
  @*/

/*@ requires \valid(q1+(0..3));
  @ requires \valid(q2+(0..3));
  @ requires \valid(q+(0..3));
  @ assigns q[0..3];
  @ ensures is_product(q1,q2,q);
  @*/
void Quat_prod(const quat q1, const quat q2, quat q) {
  q[0] = q1[0]*q2[0] - q1[1]*q2[1] - q1[2]*q2[2] - q1[3]*q2[3];
  q[1] = q1[0]*q2[1] + q1[1]*q2[0] + q1[2]*q2[3] - q1[3]*q2[2];
  q[2] = q1[0]*q2[2] - q1[1]*q2[3] + q1[2]*q2[0] + q1[3]*q2[1];
  q[3] = q1[0]*q2[3] + q1[1]*q2[2] - q1[2]*q2[1] + q1[3]*q2[0];
}



/*************** Quaternions: random ******************/

/*@ logic real quat_norm{L}(quat_ptr q) =
  @   \sqrt(q[0]*q[0] + q[1]*q[1] + q[2]*q[2] + q[3]*q[3]);
  @*/


/*@ requires \valid(q+(0..3));
  @ assigns q[0..3];
  @ ensures quat_norm(q) == 1.0;
  @*/
void random_unit_quat(quat q);


/*************** Main loop and property ******************/

/*@ lemma norm_product{L}:
  @   \forall quat_ptr q1,q2,q; is_product(q1,q2,q) ==>
  @     quat_norm(q) == quat_norm(q1) * quat_norm(q2);
  @*/


int test1(void) {

  double current[4], next[4], incr[4];

  random_unit_quat(current);

  /*@ loop invariant quat_norm(current+0) == 1.0;
    @*/
  while (1) {
    random_unit_quat(incr);
    Quat_prod(current,incr,next);
    //@ assert quat_norm(next+0) == 1.0;
    Quat_copy(next,current);
  }
  return 0;
}




/*i
Local Variables:
compile-command: "frama-c -jessie quat1.c"
End:
*/

/**************************************************************************/
/*                                                                        */
/*  The Why platform for program certification                            */
/*                                                                        */
/*  Copyright (C) 2002-2017                                               */
/*                                                                        */
/*    Jean-Christophe FILLIATRE, CNRS & Univ. Paris-sud                   */
/*    Claude MARCHE, INRIA & Univ. Paris-sud                              */
/*    Yannick MOY, Univ. Paris-sud                                        */
/*    Romain BARDOU, Univ. Paris-sud                                      */
/*                                                                        */
/*  Secondary contributors:                                               */
/*                                                                        */
/*    Thierry HUBERT, Univ. Paris-sud  (former Caduceus front-end)        */
/*    Nicolas ROUSSET, Univ. Paris-sud (on Jessie & Krakatoa)             */
/*    Ali AYAD, CNRS & CEA Saclay      (floating-point support)           */
/*    Sylvie BOLDO, INRIA              (floating-point support)           */
/*    Jean-Francois COUCHOT, INRIA     (sort encodings, hyps pruning)     */
/*    Mehdi DOGGUY, Univ. Paris-sud    (Why GUI)                          */
/*                                                                        */
/*  This software is free software; you can redistribute it and/or        */
/*  modify it under the terms of the GNU Lesser General Public            */
/*  License version 2.1, with the special exception on linking            */
/*  described in file LICENSE.                                            */
/*                                                                        */
/*  This software is distributed in the hope that it will be useful,      */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  */
/**************************************************************************/

/*@ allocates \result;
  @ ensures \fresh(\result,size) && \valid(\result+(0..size-1));
  @*/
char *malloc(unsigned int size);

typedef struct Fresh { int f; } *fresh;

/*@ allocates \result;
  @ ensures \fresh(\result,sizeof(struct Fresh)) && \valid(\result);
  @*/
fresh create() {
  fresh f = (fresh)malloc(sizeof(struct Fresh));
  return f;
}

/*@ requires \valid(f1);
  @*/
void test (fresh f1) {
  fresh f2 = create ();
  //@ assert f1 != f2;
  //@ assert \valid(f2);
  // assert \false;
}


/*
Local Variables:
compile-command: "make fresh.why3ml"
End:
*/



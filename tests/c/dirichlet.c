/**************************************************************************/
/*                                                                        */
/*  The Why platform for program certification                            */
/*                                                                        */
/*  Copyright (C) 2002-2017                                               */
/*                                                                        */
/*    Jean-Christophe FILLIATRE, CNRS & Univ. Paris-sud                   */
/*    Claude MARCHE, INRIA & Univ. Paris-sud                              */
/*    Yannick MOY, Univ. Paris-sud                                        */
/*    Romain BARDOU, Univ. Paris-sud                                      */
/*                                                                        */
/*  Secondary contributors:                                               */
/*                                                                        */
/*    Thierry HUBERT, Univ. Paris-sud  (former Caduceus front-end)        */
/*    Nicolas ROUSSET, Univ. Paris-sud (on Jessie & Krakatoa)             */
/*    Ali AYAD, CNRS & CEA Saclay      (floating-point support)           */
/*    Sylvie BOLDO, INRIA              (floating-point support)           */
/*    Jean-Francois COUCHOT, INRIA     (sort encodings, hyps pruning)     */
/*    Mehdi DOGGUY, Univ. Paris-sud    (Why GUI)                          */
/*                                                                        */
/*  This software is free software; you can redistribute it and/or        */
/*  modify it under the terms of the GNU Lesser General Public            */
/*  License version 2.1, with the special exception on linking            */
/*  described in file LICENSE.                                            */
/*                                                                        */
/*  This software is distributed in the hope that it will be useful,      */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  */
/**************************************************************************/


/*@ axiomatic dirichlet {
  @    predicate analytic_error{L}
  @          (double **p, integer ni, integer i, integer k, double a)
  @          reads p[..][..];
  @ }
  @*/


/*@  predicate separated_matrix(double **p,integer leni) =
  @     \forall integer i; \forall integer j;
  @          0 <= i < leni ==> 0 <= j < leni ==> i != j ==>
  @          \base_addr(p[i]) != \base_addr(p[j]);
  @*/


/*@ requires leni >= 1;
  @ requires lenj >= 1;
  @ ensures \valid(\result + (0 .. leni-1));
  @ ensures \forall integer i; 0 <= i < leni ==>
  @               \valid(\result[i] + (0 .. lenj-1));
  @ ensures separated_matrix(\result,leni);
  @ */
double **array2d_alloc(int leni, int lenj);


/*@ requires (l != 0);
  @ ensures \round_error(\result) <= 14 * 0x1.p-52;
  @ ensures \abs(\result) <= 2.0;
  @ */
double p_zero(double xs, double l, double x);

#define MAX 1023

/*@ requires 2 <= ni < 0x7FFFFFFF && 2 <= nk <= 4194304;
  @ requires l != 0;
  @ requires dx > 0.0 && dt > 0.0  && v > 0.0;
  @ requires \exact(dx) > 0.0 && \exact(dt) > 0.0;
  @ requires \exact(v)==v;
  @ requires \abs(\exact(dx) - dx) / dx <= 0x1.p-53;
  @ requires \abs(\exact(dt) - dt) / dt <= 0x1.p-51;
  @ requires 3.0/5.0 <= \exact(dt) / \exact(dx) * \exact(v) <= 1 - 0x1.p-50;
  @ requires 0x1.p-1000 <= v <= 0x1.p1000;
  @ requires \exact(dx) <= 1;
  @
  @ ensures \forall integer i, k;
  @     0 <= i <= ni && 0 <= k <= nk ==>
  @     \round_error(\result[i][k]) <= 78.0/2.0 * 0x1.p-52 * (k+1) * (k+2);
  @*/
double **forward_prop(int ni, int nk, double dx, double dt, double v,
                      double xs, double l) {

  /* output variable */
  double **p;

  /* local variables */
  int i, k;
  double a1, a, dp;


  /* assemble diagonal of stiffness matrix M[v]^{-1}A */
  a1 =  dt/dx*v;
  a  = a1*a1;

  //@ assert 0.25 <= a <= 1.0 ;
  //@ assert 0.0 < \exact(a) <= 1.0 ;
  //@ assert \round_error(a) <= 0x1.p-49;

  p = array2d_alloc(ni+1, nk+1);
  /* initial conditions (includes boundary conditions) */
  //@ assert \valid(p[0]+0);
  p[0][0] = 0.0;
  /*@ loop invariant 1 <= i <= ni;
    @ loop invariant analytic_error(p,ni,i-1,0,a);
    @ loop variant ni-i;
    @*/
  for (i=1; i<ni; i++) {
    //@ assert \valid(p[i]+0);
    p[i][0] = p_zero(xs, l, i*dx);
  }
  //@ assert \valid(p[ni]+0);
  p[ni][0] = 0.0 ;

  /*@ assert analytic_error(p,ni,ni,0,a); */

  /* initial time derivative is supposed null + boundary conditions */

  //@ assert \valid(p[0]+1);
  p[0][1] = 0.0 ;

  /*@ loop invariant 1 <= i <= ni;
    @ loop invariant analytic_error(p,ni,i-1,1,a);
    @ loop variant ni-i;
    @*/
  for (i=1; i<ni; i++) {
    //@ assert \valid(p[i-1]+0);
    //@ assert \valid(p[i]+0);
    //@ assert \valid(p[i+1]+0);
    dp = p[i+1][0] - 2.*p[i][0] + p[i-1][0];
    // assert \valid(p[i]+1);
    p[i][1] = p[i][0] + 0.5*a*dp;
  }
  //@ assert \valid(p[ni]+1);
  p[ni][1] = 0.;

  /*@ assert analytic_error(p,ni,ni,1,a); */

  /* propagation = time loop */
  /*@ loop invariant 1 <= k <= nk;
    @ loop invariant analytic_error(p,ni,ni,k,a);
    @ loop variant nk-k;
    @*/
  for (k=1; k<nk; k++) {
    /* boundary condition 1 */
    p[0][k+1] = 0.0;

    /* time iteration = space loop */
    /*@ loop invariant 1 <= i <= ni;
      @ loop invariant analytic_error(p,ni,i-1,k+1,a);
      @ loop variant ni-i;
      @*/
    for (i=1; i<ni; i++) {
      dp = p[i+1][k] - 2.*p[i][k] + p[i-1][k];
      p[i][k+1] = 2.*p[i][k] - p[i][k-1] + a*dp;
    }

    /* boundary condition 2 */
    p[ni][k+1] = 0.0;

    /*@ assert analytic_error(p,ni,ni,k+1,a); */

  }

  return p;

}

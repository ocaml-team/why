
#define G 9.807
#define Frame_Length (1.0 / 60.0)

/*@ logic real Low_Bound (integer N) = N * (-64.25);
  @*/

/*@ logic real High_Bound (integer N) = N * 64.25;
  @*/


/*@ predicate In_Bounds (double V) =
  @   -65.0 * 25000.0 <= V <= 65.0 * 25000.0;
  @*/

/*@ predicate Invariant(integer N, double Speed) =
  @   Low_Bound(N) <= Speed <= High_Bound(N);
  @
  @*/

/*@ requires 0 <= N < 25000;
  @ requires -1.0 <= Factor <= 1.0;
  @ requires -64.0 <= Drag <= 64.0;
  @ requires In_Bounds(Old_Speed); // enough for proving absence of overflow
  @ requires Invariant(N,Old_Speed);
  @ ensures Invariant(N+1,\result);
  @*/
double new_speed(int N, double Factor, double Drag, double Old_Speed) {
 double Delta_Speed = Drag + Factor * G * Frame_Length;
 //@ assert  -0.1875 <= Factor * G * Frame_Length <= 0.1875;
 //@ assert  -64.1875 <= Delta_Speed <= 64.1875;
 /*@ assert Old_Speed + Delta_Speed <= N*65 + 64.1875;
   @*/
 /*@ assert \round_double(\NearestEven,Old_Speed + Delta_Speed) <= N*64.25 + 64.25;
   @*/
 /*@ assert Old_Speed + Delta_Speed >= N*-64.25 - 64.1875;
   @*/
 /*@ assert \round_double(\NearestEven,Old_Speed + Delta_Speed) >= N*-64.25 - 64.25;
   @*/
 return Old_Speed + Delta_Speed;
}


#pragma JessieFloatModel(math)

//@ ghost double gE_1, gE_2, gS_1, gS_2;

int Binit;
extern const double CI1,CI2,C1,C2,C3,C4,C5;
extern double X[], Y[];

/*@ requires N >= 2;
    requires \valid(X+(0..N-1));
    requires \valid(Y+(0..N-1));

    ensures Y[0]==CI1;
    ensures Y[1]==C1*X[1]+C2*X[0]+C3*CI2+C4*Y[0]+C5*CI1;
    ensures \forall integer i;
            2<=i<N ==> Y[i]==C1*X[i]+C2*X[i-1]+C3*X[i-2]+C4*Y[i-1]+C5*Y[i-2];
*/
void iter_FILT2(int N) {
    Binit=1;
    /*@ loop invariant 0 <= k <= N;
      @
      @ loop invariant k==0 ==> Binit==1;
      @ loop invariant k>=1 ==> Binit==0;
      @
      @ loop invariant k==1 ==> gS_2==gS_1==CI1 && gE_1==X[0] && gE_2==CI2;
      @ loop invariant k>=1 ==> Y[0]==CI1;
      @
      @ loop invariant k>=2 ==> Y[1]==C1*X[1]+C2*X[0]+C3*CI2+C4*CI1+C5*CI1;
      @ loop invariant k>=2 ==> gE_1==X[k-1] && gE_2==X[k-2] && gS_1==Y[k-1] && gS_2==Y[k-2];
      @
      @ loop invariant \forall integer i;
      @            2<=i<k ==> Y[i]==C1*X[i]+C2*X[i-1]+C3*X[i-2]+C4*Y[i-1]+C5*Y[i-2];

      @ loop variant N-k;
      @*/
    for (int k=0; k<N; k++) {
      static double E_1,E_2,S_1,S_2;
      //@ ghost E_1=gE_1; E_2=gE_2; S_1=gS_1; S_2=gS_2;
      if (Binit) {Y[k]=S_1=CI1;  E_1=CI2;}
      else {Y[k]=C1*X[k]+C2*E_1+C3*E_2+C4*S_1+C5*S_2;}
      E_2=E_1; S_2=S_1;
      E_1=X[k]; S_1=Y[k];
      //@ ghost gE_1=E_1; gE_2=E_2; gS_1=S_1; gS_2=S_2;
      Binit=0;
    }
}


/*i
Local Variables:
compile-command: "frama-c -jessie filter2fixed.c"
End:
*/

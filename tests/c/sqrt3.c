/* Définition types */
typedef unsigned short tWORD; /* MAX 65535 */
typedef unsigned char tBYTE;  /* MAX 255 */

// interpretation of a tWORD as a real number
/*@ logic real as_real(integer n) = n / 100.0;
  @*/

//@ lemma as_real_add : \forall integer x,y; as_real(x+y) == as_real(x) + as_real(y);
//@ lemma as_real_sub : \forall integer x,y; as_real(x-y) == as_real(x) - as_real(y);
//@ lemma as_real_mul : \forall integer x,y; as_real(x*y) == 100.0 * as_real(x) * as_real(y);
/*@ lemma as_real_div_down : \forall integer x,y; x >= 0 && y > 0 ==>
  @    0.01 * as_real(x) / as_real(y) - 0.01 <= as_real(x/y) ;
  @*/
/*@ lemma as_real_div_up : \forall integer x,y; x >= 0 && y > 0 ==>
  @    as_real(x/y) <= 0.01 * as_real(x) / as_real(y) ;
  @*/

/* Calcule l'ordonnée d'un point d'abscisse X à partir de 2 points déterminés de la droite (Xa,Ya) et (Xb,Yb) */

/*@ requires 0 <= Xa <= 100 && 0 <= Xb <= 100;
  @ requires 0 <= Ya <= 100 && 0 <= Yb <= 100;
  @ requires Yb > Ya && Xb >= Xa;
  @ requires Xa <= X <= Xb;
  @ //ensures Xa != Xb ==> \result == (Ya + (X - Xa) * (Yb - Ya) / (Xb - Xa));
  @ //ensures Xa == Xb ==> \result == Ya;
  @ ensures Xa != Xb ==>
  @    as_real(\result) <= as_real(Ya) +
  @      (as_real(X) - as_real(Xa)) * (as_real(Yb) - as_real(Ya)) / (as_real(Xb) - as_real(Xa));
  @ ensures Xa != Xb ==>
  @    as_real(\result) >= as_real(Ya) +
  @      (as_real(X) - as_real(Xa)) * (as_real(Yb) - as_real(Ya)) / (as_real(Xb) - as_real(Xa))
  @      - 0.01 ;
  @ assigns \nothing;
  @*/
tWORD InterpolLineaire(tWORD Xa, tWORD Ya, tWORD Xb, tWORD Yb, tWORD X)
{
	if (Xa != Xb)
	{
          /*@ assert as_real((X - Xa) * (Yb - Ya) / (Xb - Xa)) <=
            @   0.01 * as_real((X - Xa) * (Yb - Ya)) / as_real (Xb-Xa);
            @*/
          //@ assert 0.01 * as_real((X - Xa) * (Yb - Ya)) == as_real(X-Xa) * as_real(Yb-Ya);
          return(Ya + (X - Xa) * (Yb - Ya) / (Xb - Xa));
	}
	else
	{
          return(Ya);
	}
}

/* Approximation de la fonction racine carré par palier */
/* 0 <= contenuRacine <= 10000 => 0 <= Resultat <= 1000 */
/* contenuRacine > 10000 => Resultat = 1000 */

tWORD TabX[8], TabY[8];

/*@ assigns \nothing;
  @ behavior racine_calculable:
  @   assumes contenuRacine <= 100;
  @   ensures as_real(contenuRacine) - 0.5 <=
  @           as_real(\result) * as_real (\result) <=
  @              as_real(contenuRacine) + 0.5;
  @ behavior racine_noncalculable:
  @   assumes contenuRacine > 100;
  @   ensures \result == 100;
  @ complete behaviors racine_calculable, racine_noncalculable;
  @ disjoint behaviors racine_calculable, racine_noncalculable;
*/
tWORD ApproximationRacine(tWORD contenuRacine)
{
	tBYTE i=0;
	tBYTE j=0;

	// On reprend les valeurs données par le tableau pour approximer la
	// racine carré Toutes les valeurs sont multipliées par 100 (pour
	// supprimer les décimales), la valeur renvoyée doit donc être divisée
	// par 100 pour retomber sur la valeur donnée dans le tableau
	/* tWORD TabX[8] = {0,5,10,25,40,55,75,100}; */
        TabX[0] = 0;
        TabX[1] = 5;
        TabX[2] = 10;
        TabX[3] = 25;
        TabX[4] = 40;
        TabX[5] = 55;
        TabX[6] = 75;
        TabX[7] = 100;

	/* tWORD TabY[8] = {0,22,32,50,63,74,87,100}; */
        TabY[0] = 0;
        TabY[1] = 22;
        TabY[2] = 32;
        TabY[3] = 50;
        TabY[4] = 63;
        TabY[5] = 74;
        TabY[6] = 87;
        TabY[7] = 100;

        /*@ loop invariant 0 <= i <= 7 && contenuRacine >= TabX[i];
          @ loop assigns \nothing; // not i for jessie
          @ loop variant 7-i; */
	for (i = 0 ; i < 7 ; i++)
	{
          if ((contenuRacine >= TabX[i]) && (contenuRacine <= TabX[i+1]))
		{
                  // Interpolation linéaire entre les points d'abcisses TabX[i] et TabX[i+1]
                  return(InterpolLineaire(TabX[i], TabY[i], TabX[i+1], TabY[i+1], contenuRacine));
		}
	}

	return TabY[7];
}

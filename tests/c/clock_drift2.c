/**************************************************************************/
/*                                                                        */
/*  The Why platform for program certification                            */
/*                                                                        */
/*  Copyright (C) 2002-2017                                               */
/*                                                                        */
/*    Jean-Christophe FILLIATRE, CNRS & Univ. Paris-sud                   */
/*    Claude MARCHE, INRIA & Univ. Paris-sud                              */
/*    Yannick MOY, Univ. Paris-sud                                        */
/*    Romain BARDOU, Univ. Paris-sud                                      */
/*                                                                        */
/*  Secondary contributors:                                               */
/*                                                                        */
/*    Thierry HUBERT, Univ. Paris-sud  (former Caduceus front-end)        */
/*    Nicolas ROUSSET, Univ. Paris-sud (on Jessie & Krakatoa)             */
/*    Ali AYAD, CNRS & CEA Saclay      (floating-point support)           */
/*    Sylvie BOLDO, INRIA              (floating-point support)           */
/*    Jean-Francois COUCHOT, INRIA     (sort encodings, hyps pruning)     */
/*    Mehdi DOGGUY, Univ. Paris-sud    (Why GUI)                          */
/*                                                                        */
/*  This software is free software; you can redistribute it and/or        */
/*  modify it under the terms of the GNU Lesser General Public            */
/*  License version 2.1, with the special exception on linking            */
/*  described in file LICENSE.                                            */
/*                                                                        */
/*  This software is distributed in the hope that it will be useful,      */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  */
/**************************************************************************/

// RUNGAPPA: will ask regtests to run gappa on VCs of this program

#define NMAX 1000000
#define NMAXR 1000000.0

// this one is needed by Gappa to prove the assertion on the rounding error
/*@ lemma real_of_int_inf_NMAX:
  @   \forall integer i; i <= NMAX ==> i <= NMAXR;
  @*/

// this one does not seem useful for Alt-Ergo
// but it is for CVC3, to prove preservation of the loop
// invariant. Z3 does not prove it either
//@ lemma real_of_int_succ: \forall integer n; n+1 == n + 1.0;

/*@ requires 0 <= n <= NMAX;
  @ //ensures \result <= \round_double(\Up,n * 0x1p-8);
  @*/
double f_double(int n)
{
  double t = 0.0;
  int i;

  /*@ loop invariant 0 <= i <= n;
    @ //loop invariant 0.0 <= t ;
    @ //loop invariant t <= \round_double(\Up,i * 0x1p-8);
    @ loop variant n-i;
    @*/
  for(i=0;i<n;i++) {
  L:
    // assert 0.0 <= t <= NMAXR * 0x1p-8 ;
    t = t + 0.1;
    // assert t <= \at(t,L) + 0x1.Ap-4;
    // assert t <= \round_double(\Up,\at(t,L) + 0x1.Ap-4);
  }
  return t;
}


/*
Local Variables:
compile-command: "make clock_drift2.why3ide"
End:
*/

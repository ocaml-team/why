/* D�finition types */
typedef unsigned short tWORD; /* MAX 65535 */
typedef unsigned char tBYTE;  /* MAX 255 */

/* Calcule l'ordonn�e d'un point d'abscisse X � partir de 2 points d�termin�s de la droite (Xa,Ya) et (Xb,Yb) */

/*@ requires 0 <= Xa <= 10000 && 0 <= Xb <= 10000;
  @ requires 0 <= Ya <= 1000 && 0 <= Yb <= 1000;
  @ requires Yb > Ya && Xb >= Xa;
  @ requires Xa <= X <= Xb;
  @ ensures Xa != Xb ==> \result == (Ya + (X - Xa) * (Yb - Ya) / (Xb - Xa));
  @ ensures Xa == Xb ==> \result == Ya;
  @ assigns \nothing;
  @*/
tWORD InterpolLineaire(tWORD Xa, tWORD Ya, tWORD Xb, tWORD Yb, tWORD X)
{
  // assert \forall integer x,y,z; 0 < z && x <= y ==> x / z <= y / z;
  // assert \forall integer x,y; 0 < x ==> (x*y)/x == y;
	if (Xa != Xb)
	{
          /* Assert n�cessaire pour alt-ergo 1.01 mais pas 1.30 */
          /* assert 0 <= (X - Xa) * (Yb - Ya) <= (Xb-Xa) * (Yb-Ya); */
          /* assert 0 <= (X - Xa) * (Yb - Ya) / (Xb - Xa) <= (Yb-Ya); */
          return(Ya + (X - Xa) * (Yb - Ya) / (Xb - Xa));
	}
	else
	{
          return(Ya);
	}
}

#if 0

/* Approximation de la fonction racine carr� par palier */
/* 0 <= contenuRacine <= 10000 => 0 <= Resultat <= 1000 */
/* contenuRacine > 10000 => Resultat = 1000 */

/*@ assigns \nothing;
  @ behavior racine_calculable:
  @   assumes contenuRacine <= 10000;
  @   ensures contenuRacine-30 <= (\result)*(\result)/100 <= contenuRacine+10;  // en r�alit� l'erreur est de -28 � +6
  @ behavior racine_noncalculable:
  @   assumes contenuRacine > 10000;
  @   ensures \result == 1000;
  @ complete behaviors racine_calculable, racine_noncalculable;
  @ disjoint behaviors racine_calculable, racine_noncalculable;
*/
tWORD ApproximationRacine(tWORD contenuRacine)
{
	tBYTE i=0;
	tBYTE j=0;

	// On reprend les valeurs donn�es par le tableau pour approximer la
	// racine carr� Toutes les valeurs sont multipli�es par 100 (pour
	// supprimer les d�cimales), la valeur renvoy�e doit donc �tre divis�e
	// par 100 pour retomber sur la valeur donn�e dans le tableau
	tWORD TabX[41] = {0,5,10,25,40,55,75,100,125,155,185,230,290,360,460,600,700,900,1100,1300,1500,1700,2000,2300,2600,2900,3200,3600,3900,4300,4700,5100,5600,6000,6500,7000,7500,8000,8600,9200,10000};
	tWORD TabY[41] = {0,22,32,50,63,74,87,100,112,124,136,152,170,190,214,245,265,300,332,361,387,412,447,480,510,539,566,600,624,656,686,714,748,775,806,837,866,894,927,959,1000};

        /*@ loop invariant 0 <= i <= 40 && contenuRacine >= TabX[i];
          @ loop assigns i;
          @ loop variant 40-i; */
	for (i = 0 ; i < 40 ; i++)
	{
          if ((contenuRacine >= TabX[i]) && (contenuRacine <= TabX[i+1]))
		{
                  // Interpolation lin�aire entre les points d'abcisses TabX[i] et TabX[i+1]
                  return(InterpolLineaire(TabX[i], TabY[i], TabX[i+1], TabY[i+1], contenuRacine));
		}
	}

	return TabY[40];
}

#endif

/* This example is 100% proved with Frama-C/Jessie
   with the default FP model (i.e strict IEEE-754, defensive)
   
   20 VCs.
   18 proved by Gappa, except the 2 corresponding to result < f+1
   and f-1 < result. The latter are porved by Alt-Ergo and Z3


   Done in Frama-C/Why to make it work:

   in Why lib:
   * addition of symbol truncate_real_to_int in Why real theory:
   * rounds to zero, axiomatized by 2 axioms (see lib/why/real.why)

   in Why, Gappa output:
   * add support for this in Gappa output for Why: this function is
     mapped to int<zr> (see
     http://gappa.gforge.inria.fr/doc/index.html, fixed-point
     operators )

   in Jessie:
   * appropriate translation of double-to-int cast into truncate_real_to_int
   * rounding of constants done "at compile-time" (TODO: use Mpfr for that)


   TODO: propose an alternate specification taking into account
   possible rounding error on input f

*/


/*@
behavior small:
    assumes f <= -65536.0;
    ensures \result == -65536;

behavior big:
    assumes f >= 65535.0;
    ensures \result == 65535;

behavior negative_fits:
    assumes -65536.0 < f < 0.0;
    ensures f <= \result < f+1;

behavior positive_fits:
    assumes 0.0 <= f < 65535.0;
    ensures f-1 < \result <= f;
*/
int double2int(double f) {
    int i;
    if      (f <= -65536.0)  i=-65536;
    else if (f >= 65535.0)   i=65535;
    else                    i=f;
    return i;
}


/*i
Local Variables: 
compile-command: "frama-c -jessie double2int.c"
End: 
*/

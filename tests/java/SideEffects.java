/**************************************************************************/
/*                                                                        */
/*  The Why platform for program certification                            */
/*                                                                        */
/*  Copyright (C) 2002-2017                                               */
/*                                                                        */
/*    Jean-Christophe FILLIATRE, CNRS & Univ. Paris-sud                   */
/*    Claude MARCHE, INRIA & Univ. Paris-sud                              */
/*    Yannick MOY, Univ. Paris-sud                                        */
/*    Romain BARDOU, Univ. Paris-sud                                      */
/*                                                                        */
/*  Secondary contributors:                                               */
/*                                                                        */
/*    Thierry HUBERT, Univ. Paris-sud  (former Caduceus front-end)        */
/*    Nicolas ROUSSET, Univ. Paris-sud (on Jessie & Krakatoa)             */
/*    Ali AYAD, CNRS & CEA Saclay      (floating-point support)           */
/*    Sylvie BOLDO, INRIA              (floating-point support)           */
/*    Jean-Francois COUCHOT, INRIA     (sort encodings, hyps pruning)     */
/*    Mehdi DOGGUY, Univ. Paris-sud    (Why GUI)                          */
/*                                                                        */
/*  This software is free software; you can redistribute it and/or        */
/*  modify it under the terms of the GNU Lesser General Public            */
/*  License version 2.1, with the special exception on linking            */
/*  described in file LICENSE.                                            */
/*                                                                        */
/*  This software is distributed in the hope that it will be useful,      */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                  */
/**************************************************************************/

//@+ CheckArithOverflow = no

class SideEffects {

    /*@ requires t.length > 10;
      @*/
    void m1(int t[]) {
	int i = 0;
	t[i++] = 1;
	//@ assert t[0] == 1 && i == 1;
	t[++i] = 2;
	//@ assert t[0] == 1 && t[2] == 2 && i == 2;
	t[--i] = 3;
	//@ assert t[0] == 1 && t[2] == 2 && t[1] == 3 && i == 1;
	t[i--] = 4;
	//@ assert t[0] == 1 && t[2] == 2 && t[1] == 4 && i == 0;
    }

}


/*
Local Variables:
compile-command: "make SideEffects.why3ide"
End:
*/



#!/bin/bash

JAVA="ArrayMax Arrays BinarySearch Counter Fact Fibonacci FlagStatic \
      Gcd Isqrt MacCarthy Muller MyCosine Negate Purse Sort Sort2 \
      SelectionSort Termination TreeMax"

JAVATESTS="AllZeros Assertion Creation Fresh Fresh2 Fresh3 Hello \
      Literals NameConflicts PreAndOld SideEffects Switch TestNonNull"

JAVAUNFINISHED="Power SimpleAlloc"

JAVATODO="Duplets SimpleApplet"

C="array_double array_max binary_search clock_drift clock_drift2 \
   double_rounding_strict_model double_rounding_multirounding_model \
   double2int \
   duplets eps_line1 eps_line2 exp fact filter2fixed \
   find_array flag floats_bsearch \
   float_sqrt insertion_sort interpol \
   isqrt minmax muller my_cosine power \
   quat1 quat2 \
   rec reverse_array scalar_product selection_sort Sterbenz sum_array \
   swap tree_max"

CTESTS="binary_search_wrong"

CUNFINISHED="cd1d conjugate float_array heap_sort interval_arith \
   overflow_level quick_sort sparse_array2"

CTODO="binary_heap maze popHeap sparse_array"

FRAMA="bts1251 floor fresh2 fresh2_sep hollas_floats power strprevchr \
       binary-search-hollas bts12891 jessie_label_bug bool_bug \
       q19_switch_fun_call"


case "$1" in
  "--force")
        REPLAYOPT="--force"
        ;;
  "--obsolete-only")
        REPLAYOPT="--obsolete-only"
        ;;
  "")
        REPLAYOPT=""
        ;;
  *)
        echo "$0: Unknown option '$1'"
        exit 2
esac


DIR=$PWD
TMP=$DIR/jessie2-regtests.out
TMPERR=$DIR/jessie2-regtests.err
export WHYLIB=$DIR/lib
export KRAKATOALIB=$DIR/lib

res=0
export success=0
export total=0

report_error () {
    printf "$2 FAILED (ret code=$1)\n"
    printf "standard error:\n"
    cat $TMPERR
    printf "standard output:\n"
    cat $TMP
    res=1
}

cd tests


run_java () {
    printf "$1.java... "
    ../../bin/krakatoa.opt -gen-only $1.java 2> $TMPERR > $TMP
    ret=$?
    if test "$ret" != "0"  ; then
	report_error $ret "krakatoa"
    else
        ../../bin/jessie.opt -gen-only -locs $1.jloc $1.jc 2> $TMPERR > $TMP
        ret=$?
        if test "$ret" != "0"  ; then
	    report_error $ret "jessie"
        else
            why3 replay $REPLAYOPT --extra-config $WHYLIB/why3/why3.conf $1 2> $TMPERR > $TMP
            ret=$?
            if test "$ret" != "0"  ; then
	        printf "replay FAILED (ret code=$ret):"
                out=`head -1 $TMP`
                if test -z "$out" ; then
                    printf "standard error: (standard output empty)"
                    cat $TMPERR
                else
	            cat $TMP
                fi
	        res=1
	    else
	        printf "OK"
	        cat $TMP
                success=`expr $success + 1`
	    fi
        fi
    fi
    total=`expr $total + 1`
}

run_c () {
    printf "$1.c... "
    # -load-module $2/frama-c-plugin/top/Jessie.cmxs
    frama-c -add-path $2/frama-c-plugin -jessie -jessie-gen-only $1.c 2> $TMPERR > $TMP
    ret=$?
    if test "$ret" != "0"; then
	report_error $ret "frama-c -jessie"
    else
       $2/bin/jessie.opt -gen-only -locs $1.jessie/$1.cloc $1.jessie/$1.jc 2> $TMPERR > $TMP
       ret=$?
       if test "$ret" != "0"; then
	   report_error $ret "jessie"
        else
            why3 replay $REPLAYOPT --extra-config $WHYLIB/why3/why3.conf $1.jessie/$1 2> $TMPERR > $TMP
            ret=$?
            if test "$ret" != "0"  ; then
	        printf "replay FAILED (ret code=$ret):"
                out=`head -1 $TMP`
                if test -z "$out" ; then
                    printf "standard error: (standard output empty)"
                    cat $TMPERR
                else
	            cat $TMP
                fi
	        res=1
	    else
	        printf "OK"
	        cat $TMP
                success=`expr $success + 1`
	    fi
        fi
    fi
    total=`expr $total + 1`
}

printf "\n-------- Java examples --------\n"

cd java

for i in $JAVA; do
    run_java $i
done

printf "\n-------- Java unfinished examples --------\n"

for i in $JAVAUNFINISHED; do
    run_java $i
done

printf "\n-------- Java tests --------\n"

for i in $JAVATESTS; do
    run_java $i
done

cd ..

printf "\n-------- C examples --------\n"

cd c

for i in $C; do
    run_c $i "../.."
done

printf "\n-------- C unfinished examples --------\n"

for i in $CUNFINISHED; do
    run_c $i "../.."
done


printf "\n-------- C tests --------\n"

for i in $CTESTS; do
    run_c $i "../.."
done

cd ../../frama-c-plugin/tests/jessie

for i in $FRAMA; do
    run_c $i "../../.."
done

cd ../../../tests

echo ""
echo "------------ Summary ------------"
echo "Summary       : $success/$total"


exit $res

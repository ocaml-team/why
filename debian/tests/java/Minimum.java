public class Minimum {

    /*@ ensures (\result >= i && \result >= j);  
      @*/
    
    public static int max(int i, int j) { 
	if (i < j) return j; else return i; 
    }
}

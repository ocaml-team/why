let coqversion = "v8"
let version = "2.40"
let bindir = "/usr/local/bin"
let libdir = "/usr/local/lib/why"
let banner : ('a,'b,'c) format = "This is %s version %s@\nCopyright (c) 2006-2017 - Why dev team - CNRS & Inria & Univ Paris-Sud@\nThis is free software with ABSOLUTELY NO WARRANTY@."
